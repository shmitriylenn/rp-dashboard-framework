package com.epam.dashboard.tests;

import com.epam.dashboard.model.User;
import com.epam.dashboard.pages.LoginPage;
import com.epam.dashboard.services.UserCreator;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTests extends CommonConditions {

	@Test
	public void loginReportPortal() {
		User testUser = UserCreator.withCredentialsFromProperty();
		String loggedInUserName = new LoginPage(driver)
				.openPage()
				.login(testUser)
				.getAdminName();
		Assert.assertEquals(loggedInUserName.toLowerCase(), testUser.getUsername());
	}
}
