package com.epam.dashboard.pages;

import com.epam.dashboard.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends AbstractPage {

	private final Logger logger = LogManager.getRootLogger();
	private final String PAGE_URL = "http://localhost:8080/ui/#login";

	@FindBy(xpath = "//input[@name='login']")
	public WebElement inputLogin;

	@FindBy(xpath = "//input[@name='password']")
	public WebElement inputPassword;

	@FindBy(xpath = "//button[@type='submit']")
	public WebElement loginButton;

	public LoginPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(this.driver, this);
	}

	@Override
	public LoginPage openPage() {
		driver.get(PAGE_URL);
		logger.info("Login page opened");
		return this;
	}

	public DashboardPage login(User user) {
		inputLogin.sendKeys(user.getUsername());
		inputPassword.sendKeys(user.getPassword());
		loginButton.click();
		logger.info("Login performed");
		return new DashboardPage(driver);
	}
}
