package com.epam.dashboard.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public abstract class AbstractPage {
	protected WebDriver driver;

	protected abstract AbstractPage openPage();

	protected final int WAIT_TIMEOUT_SECONDS = 10;

	protected AbstractPage(WebDriver driver) {
		this.driver = driver;
	}

	public void waitClickableForWebElement(WebElement element, int timeOut) {
		new WebDriverWait(driver, Duration.ofSeconds(timeOut))
				.until(ExpectedConditions.elementToBeClickable(element));

	}

	public void waitInvisibilityForWebElement(WebElement element, int timeOut) {
		new WebDriverWait(driver, Duration.ofSeconds(timeOut))
				.until(ExpectedConditions.invisibilityOf(element));

	}
}
