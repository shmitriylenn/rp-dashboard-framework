package com.epam.dashboard.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class DashboardPage extends AbstractPage {

	private final String DASHBOARD_URL = "http://localhost:8080";
	@FindBy(xpath = "//div[contains(@class, 'userBlock__avatar-wrapper')]")
	public WebElement adminIcon;
	@FindBy(xpath = "//div[contains(@class, 'notificationItem__message-container')]/p")
	public WebElement notification;
	private final By adminName = By.xpath("//div[contains(@class, 'userBlock__username')]");

	public DashboardPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(this.driver, this);
	}

	@Override
	protected DashboardPage openPage() {
		driver.get(DASHBOARD_URL);
		return this;
	}

	public String getAdminName() {
		waitClickableForWebElement(adminIcon, 10);
		waitInvisibilityForWebElement(notification, 10);
		adminIcon.click();
		WebElement loggedInReportPortal = new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT_SECONDS)).until(
				ExpectedConditions.presenceOfElementLocated(adminName));
		return loggedInReportPortal.getText();
	}
}
