package com.epam.dashboard.services;

import com.epam.dashboard.model.User;
import com.epam.dashboard.util.Constants;

public class UserCreator {
	public static final String USER_NAME = "username";
	public static final String USER_PASSWORD = "password";

	public static User withCredentialsFromProperty() {
		TestDataReader.readProperties(Constants.CONFIGURATION_FILEPATH);
		return new User(TestDataReader.getProperty(USER_NAME), TestDataReader.getProperty(USER_PASSWORD));
	}
}
